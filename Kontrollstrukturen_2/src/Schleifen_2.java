import java.util.Scanner;

public class Schleifen_2 {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Wie viel Geld investieren Sie?");
		double invest = tastatur.nextDouble();
		System.out.println("Wie hoch ist der j�hrliche Zinssatz? (in %)");
		double zins = tastatur.nextDouble();
		zins = zins / 100;
		zins++;
		int jahr = 0;
		
		if(zins >= 0) {
			while(invest < 1000000.0) {
				invest = invest * zins;
				jahr++;
			}
		System.out.println("Sie sind Million�r! Und das nach nur " + jahr + " Jahren!");
		}
		else { System.out.println("Der Zins darf nicht <= 0 sein!"); }
	}

}
