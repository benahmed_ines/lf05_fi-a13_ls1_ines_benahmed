import java.util.Scanner;

public class Auswahlstrukturen {
	public static void main(String[] args) {
		
		  bmiBerechnen();
	}
		public static void bmiBerechnen() {
			
			// BMI Tabelle
			
			System.out.println("   BMI (Body Mass Index)\n");  
			System.out.println("Klassifikation |   m   |  w");  
			System.out.println("------------------------------");  
			System.out.println("Untergewicht   | <20   | <19");  
			System.out.println("Normalgewicht  | 20-25 | 19-24");  
			System.out.println("�bergewicht    |  >25  | >24\n");  
			System.out.println("\n\n");
			
		 	Scanner tastatur = new Scanner(System.in);
		 	
			char Geschlecht;
			double Gewicht;
			double Groesse;
			
			// Auswahl des Geschlechts
			
			System.out.println("Bitte geben Sie Ihr Geschlecht an!");  
			System.out.println("W f�r weiblich oder M f�r m�nnlich.");
			Geschlecht = tastatur.next().charAt(0);
			
			if(Geschlecht == 'w' || Geschlecht == 'm')
			{	// BMI Berechnung 
				
				System.out.println("Bitte geben Sie nun Ihre K�rpergr��e in Metern an!");
			 	  Groesse = tastatur.nextDouble();
			 	  
			 	 System.out.println("Bitte geben Sie nun Ihr Gewicht in Kilogramm an!");
			 	  Gewicht = tastatur.nextDouble();
				
				  double BMI = Gewicht/(Groesse*Groesse);
				  System.out.println("Dein BMI ist: " + BMI);
				  			
		}
			else
			{	System.out.println("Fehlerhafte Eingabe!");
			
			}
		
			
		}
		
	}

		
		
