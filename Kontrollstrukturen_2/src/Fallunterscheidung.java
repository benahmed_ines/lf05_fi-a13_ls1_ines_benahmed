import java.util.Scanner;
public class Fallunterscheidung
{
 public static void main(String[] args)
 {
 Scanner myScanner = new Scanner(System.in);
 
 // Taschenrechner

 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

 int zahl1 = myScanner.nextInt();

 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

 int zahl2 = myScanner.nextInt();

 double ergebnis1 = zahl1 + zahl2;
 double ergebnis2 = zahl1 - zahl2;
 double ergebnis3 = zahl1 * zahl2;
 double ergebnis4 = zahl1 / zahl2;
 
 char zeichen;
 
 System.out.println("Bitte geben Sie eines der folgenden Rechenzeichen ein! ");
 System.out.println("+ oder - oder * oder /");
 
 zeichen = myScanner.next().charAt(0);
 
 switch (zeichen)
 {
 case '+':
	 System.out.print("\n\n\nErgebnis der Addition lautet: ");
	 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis1);
	 break;
	 
 case '-':
	 System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
	 System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis2);
	 break;
	 
 case '*':
	 System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
	 System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis3);
	 break;
	 
 case '/':
	 System.out.print("\n\n\nErgebnis der Division lautet: ");
	 System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnis4);
	 break;	
	 
default: 
	 System.out.print("Fehlerhafte Eingabe!");
	 break;
 }
 





 myScanner.close();

 }
}