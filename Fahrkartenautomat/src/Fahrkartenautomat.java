﻿import java.util.Scanner;

class Fahrkartenautomat
{	
    public static void main(String[] args)
    {
    
       double zuZahlenderBetrag;
       double rückgabebetrag_inCent;
       
       while(true) {
       
    	 // Preis errechnen
         zuZahlenderBetrag = fahrkartenbestellungErfassen();	
           
         // Geld einwerfen
         rückgabebetrag_inCent = fahrkartenBezahlen(zuZahlenderBetrag); 		
           
         // Fahrkarten ausgeben
         fahrkartenAusgeben();

         // Rückgeld berechnen und ausgeben															
         rueckgeldAusgeben(rückgabebetrag_inCent);
           
         // Abschluss
         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                            "vor Fahrtantritt entwerten zu lassen!\n"+
                            "Wir wünschen Ihnen eine gute Fahrt.\n\n");
       }
    }
    
    
    
    // Aufgabe 3.3 - Methoden im Fahrkartenautomat verwenden
    
    public static double fahrkartenbestellungErfassen() {
    	// Bestellung des Käufers erfassen
    	
    	Scanner scanner = new Scanner(System.in);
    	int ticket;
    	double einzelpreis;
    	
    	double[] preise=new double[10];
    	String[] tickets=new String[10];
    	
    	// Preise Array befüllen
    	preise[0]= 2.90;
    	preise[1]= 3.30;
    	preise[2]= 3.60;
    	preise[3]= 1.90;
    	preise[4]= 8.60;
    	preise[5]= 9.00;
    	preise[6]= 9.60;
    	preise[7]= 23.50;
    	preise[8]= 24.30;
    	preise[9]= 24.90;
    	
    	// Tickets Array befüllen
    	tickets[0]= "Einzelfahrschein Berlin AB";
    	tickets[1]= "Einzelfahrschein Berlin BC";
    	tickets[2]= "Einzelfahrschein Berlin ABC";
    	tickets[3]= "Kurzstrecke";
    	tickets[4]= "Tageskarte Berlin AB";
    	tickets[5]= "Tageskarte Berlin BC";
    	tickets[6]= "Tageskarte Berlin ABC";
    	tickets[7]= "Kleingruppen-Tageskarte Berlin AB";
    	tickets[8]= "Kleingruppen-Tageskarte Berlin BC";
    	tickets[9]= "Kleingruppen-Tageskarte Berlin ABC";
    	
    	System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin aus: ");
    	
    	for(int e=0; e<10;e++) {
    		System.out.printf("%2d %-35s",e+1,tickets[e]);
    		System.out.printf(" %10.2f €\n", preise[e]);
    	}
    	
    	boolean done = false;
    	do {
    		System.out.print("Die gewünschte Ticketnummer: ");
    		ticket = scanner.nextInt();
    		if(ticket >= 0&& ticket<11) {
    			done = true;
    		} else {
    			System.out.println("\n>>ungültige Eingabe<<");
    			done = false;
    		}
    	} while(!done) ;
    	ticket= ticket-1;
    	einzelpreis= preise[ticket];
    	
    	System.out.print("Anzahl Tickets: ");
    	byte anzahlFahrkarten = scanner.nextByte();
    	System.out.println();
    	
    	return einzelpreis * anzahlFahrkarten;
    }
    
    // Rückgabebetrag berechnen
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
    	Scanner scanner = new Scanner(System.in);									//Objekt der Klasse Scanner erschaffen
    	
    	// lokale Variablen definieren
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
    	
    	// Berechnung
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = scanner.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        
    	return (eingezahlterGesamtbetrag*100) - (zuZahlenderBetrag*100);																	
    }
    
    public static void fahrkartenAusgeben(){
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250); 					
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag_inCent) {
    	if(rückgabebetrag_inCent > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", (rückgabebetrag_inCent/100));
     	   System.out.println("wird in folgenden Münzen ausgezahlt:\n");

            while(rückgabebetrag_inCent >= 200)
            {
         	  muenzeAusgeben(2, "EURO");
 	          rückgabebetrag_inCent -= 200;
            }
            while(rückgabebetrag_inCent >= 100)
            {
         	  muenzeAusgeben(1, "EURO");
 	          rückgabebetrag_inCent -= 100;
            }
            while(rückgabebetrag_inCent >= 50)
            {
         	  muenzeAusgeben(50, "CENT");
 	          rückgabebetrag_inCent -= 50;
            }
            while(rückgabebetrag_inCent >= 20)
            {
         	  muenzeAusgeben(20, "CENT");
  	          rückgabebetrag_inCent -= 20;
            }
            while(rückgabebetrag_inCent >= 10)
            {
         	  muenzeAusgeben(10, "CENT");
 	          rückgabebetrag_inCent -= 10;
            }
            while(rückgabebetrag_inCent >= 5)
            {
         	  muenzeAusgeben(5, "CENT");
  	          rückgabebetrag_inCent -= 5;
            }
        }
    }
    
    // Aufgabe 3.4 - Fahrkartenautomat um weitere Methoden erweitern
    
    public static void warte(int millisekunde) {
    	// unterbricht den aktuellen Thread für die übergebene Zeit in Millisekunden
    	try {
    		Thread.sleep(millisekunde);
    	} catch (InterruptedException e) {
    		e.printStackTrace();
    	}
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	// gibt den Betrag mit der genutzten Einheit (CENT oder EURO) in der Konsole aus
    	System.out.printf("%d %s \n", betrag, einheit);
    }
    
    
    
    
    
    
    
    
    
    
