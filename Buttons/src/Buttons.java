import java.awt.BorderLayout;
import java.awt.EventQueue;

import java.awt.Color;
import javax.swing.JColorChooser;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;
import javax.swing.JTextField;
public class Buttons extends JFrame {

	private JPanel contentPane;
	private JTextField txtEigenerText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Buttons frame = new Buttons();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Buttons() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 442, 872);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTextfeld = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblTextfeld.setBounds(15, 0, 398, 126);
		contentPane.add(lblTextfeld);
		
		JButton btnRed = new JButton("Rot");
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Button f�rbt Hintergrund rot
				contentPane.setBackground(Color.RED);
			}
		});
		btnRed.setBounds(20, 178, 115, 29);
		contentPane.add(btnRed);
		
		JLabel lblFarbe = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblFarbe.setBounds(20, 142, 418, 20);
		contentPane.add(lblFarbe);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.green);
			}
		});
		btnGrn.setBounds(150, 178, 115, 29);
		contentPane.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.blue);
			}
		});
		btnBlau.setBounds(280, 178, 115, 29);
		contentPane.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.yellow);
			}
		});
		btnGelb.setBounds(20, 223, 115, 29);
		contentPane.add(btnGelb);
		
		JButton btnStandartfarbe = new JButton("Standartfarbe");
		btnStandartfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(new Color(0xEEEEEE));
			}
		});
		btnStandartfarbe.setBounds(150, 223, 115, 29);
		contentPane.add(btnStandartfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
				// ausgew�hlte Farbe, Tutorial habe ich mir aus dem Internet rausgesucht,
				// da es keine Infos dazu in dem Arbeitsmaterial gibt
				
				 Color ausgewaehlteFarbe = JColorChooser.showDialog(null, 
				            "Farbauswahl", null);
				 contentPane.setBackground(ausgewaehlteFarbe);
				
			}
		});
		btnFarbeWhlen.setBounds(280, 223, 115, 29);
		contentPane.add(btnFarbeWhlen);
		
		JLabel lblSchriftart = new JLabel("Aufgabe 2: Text formatieren");
		lblSchriftart.setBounds(15, 273, 398, 20);
		contentPane.add(lblSchriftart);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextfeld.setFont(new Font("Arial",  Font.PLAIN, 12));
			}
		});
		btnArial.setBounds(20, 309, 115, 29);
		contentPane.add(btnArial);
		
		JButton btnComicSans = new JButton("Comic Sans MS");
		btnComicSans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextfeld.setFont(new Font("Comic Sans MS",  Font.PLAIN, 12));
			}
		});
		btnComicSans.setBounds(150, 309, 115, 29);
		contentPane.add(btnComicSans);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextfeld.setFont(new Font("Courier New",  Font.PLAIN, 12));
			}
		});
		btnCourierNew.setBounds(280, 309, 115, 29);
		contentPane.add(btnCourierNew);
		
		JLabel lblSchriftfarbe = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblSchriftfarbe.setBounds(15, 471, 353, 20);
		contentPane.add(lblSchriftfarbe);
		
		txtEigenerText = new JTextField();
		txtEigenerText.setText("Hier bitte Text eingeben");
		txtEigenerText.setBounds(20, 364, 375, 36);
		contentPane.add(txtEigenerText);
		txtEigenerText.setColumns(10);
		
		JButton btnEinfuegen = new JButton("Ins Label schreiben");
		btnEinfuegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextfeld.setText(txtEigenerText.getText());
				
			}
		});
		btnEinfuegen.setBounds(20, 416, 181, 29);
		contentPane.add(btnEinfuegen);
		
		JButton btnLoeschen = new JButton("Text im Label l\u00F6schen");
		btnLoeschen.addActionListener(new ActionListener() {
			// Text l�schen, der Text wird einfach gegen ein Leerzeichen ausgetauscht
			public void actionPerformed(ActionEvent arg0) {
				lblTextfeld.setText(" ");

			}
		});
		btnLoeschen.setBounds(216, 416, 179, 29);
		contentPane.add(btnLoeschen);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextfeld.setForeground(Color.red);
			}
		});
		btnRot.setBounds(20, 507, 115, 29);
		contentPane.add(btnRot);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextfeld.setForeground(Color.blue);
			}
		});
		btnBlau_1.setBounds(150, 507, 115, 29);
		contentPane.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextfeld.setForeground(Color.black);
			}
		});
		btnSchwarz.setBounds(280, 507, 115, 29);
		contentPane.add(btnSchwarz);
		
		JLabel lblAufgabeSchriftgre = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabeSchriftgre.setBounds(15, 552, 291, 20);
		contentPane.add(lblAufgabeSchriftgre);
		
		JButton btnGross = new JButton("+");
		btnGross.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Variable f�r die Schriftgr��e wird erstellt
				int groesse = lblTextfeld.getFont().getSize();
				lblTextfeld.setFont(new Font("Arial", Font.PLAIN, groesse + 1));
			}
		});
		btnGross.setBounds(20, 587, 181, 29);
		contentPane.add(btnGross);
		
		JButton btnKleiner = new JButton("-");
		btnKleiner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblTextfeld.getFont().getSize();
				lblTextfeld.setFont(new Font("Arial", Font.PLAIN, groesse - 1));
			}
		});
		btnKleiner.setBounds(216, 588, 179, 29);
		contentPane.add(btnKleiner);
		
		JLabel lblAufgabeTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeTextausrichtung.setBounds(15, 638, 313, 20);
		contentPane.add(lblAufgabeTextausrichtung);
		
		JButton btnLinksbndig = new JButton("Linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextfeld.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinksbndig.setBounds(15, 674, 115, 29);
		contentPane.add(btnLinksbndig);
		
		JButton btnRechtsbndig = new JButton("Rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextfeld.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setBounds(280, 674, 115, 29);
		contentPane.add(btnRechtsbndig);
		
		JButton btnZentriert = new JButton("Zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTextfeld.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(150, 674, 115, 29);
		contentPane.add(btnZentriert);
		
		JLabel lblProgrammBeenden = new JLabel("Aufgabe 6: Programm beenden");
		lblProgrammBeenden.setBounds(15, 725, 250, 20);
		contentPane.add(lblProgrammBeenden);
		
		JButton btnBeenden = new JButton("EXIT");
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnBeenden.setBounds(20, 757, 375, 43);
		contentPane.add(btnBeenden);
	}
}
